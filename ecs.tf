# ecs.tf

resource "aws_ecs_cluster" "main" {
  name = "test-cluster"
}

data "template_file" "locust_master" {
  template = file("./templates/ecs/locust_master.json.tpl")

  vars = {
    
    locust_port       = var.locust_port
    locust_image      = var.locust_image
    fargate_cpu       = var.fargate_cpu
    fargate_memory    = var.fargate_memory
    aws_region        = var.aws_region
  }
}

data "template_file" "locust_slave" {
  template = file("./templates/ecs/locust_slave.json.tpl")

  vars = {
    
    locust_port       = var.locust_port
    locust_image      = var.locust_image
    fargate_cpu       = var.fargate_cpu
    fargate_memory    = var.fargate_memory
    aws_region        = var.aws_region
    locust_master_host = "${aws_alb.locust_master.dns_name}"
  }
}


data "template_file" "webserver" {
  template = file("./templates/ecs/webserver.json.tpl")



  vars = {
    webserver_image      = var.webserver_image
    webserver_port       = var.webserver_port
    locust_image         = var.locust_image
    fargate_cpu          = var.fargate_cpu
    fargate_memory       = var.fargate_memory
    aws_region           = var.aws_region
  }
}

resource "aws_ecs_task_definition" "webserver" {
  family                   = "webserver-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = data.template_file.webserver.rendered
}

 

resource "aws_ecs_task_definition" "locust_master" {
  family                   = "locust_master-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = data.template_file.locust_master.rendered
}

resource "aws_ecs_task_definition" "locust_slave" {
  family                   = "locust_slave-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = data.template_file.locust_slave.rendered
}



resource "aws_ecs_service" "webserver" {
  name            = "webserver"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.webserver.arn
  desired_count   = var.webserver_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.webserver.id
    container_name   = "webserver"
    container_port   = var.webserver_port
  }


  depends_on = [aws_alb_listener.front_end, aws_iam_role_policy_attachment.ecs_task_execution_role]
}

resource "aws_ecs_service" "locust_master" {
  name            = "locust_master"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.locust_master.arn
  desired_count   = var.locust_master_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks_locust.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = false

  }
  load_balancer {
    target_group_arn = aws_alb_target_group.locust_master.id
    container_name   = "locust_master"
    container_port   = var.locust_port
  }


  depends_on = [aws_alb_listener.front_end_2, aws_iam_role_policy_attachment.ecs_task_execution_role]
}


resource "aws_ecs_service" "locust_slave" {
  name            = "locust_slave"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.locust_slave.arn
  desired_count   = var.locust_slave_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks_locust.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = false

  }

  depends_on = [ aws_iam_role_policy_attachment.ecs_task_execution_role]
}
