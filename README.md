# Hitta Terraform

Terraform config for deploying docker containers to ECS using Fargate launch type.

Terraform module that will create the resources required to run simple locust test on a webserver (jmalloc/echo-server), with atleast one master and one worker node. 


## Prerequisites

* Assume that you have access to an empty AWS account with sufficient privileges.

* AWS credentials file.
