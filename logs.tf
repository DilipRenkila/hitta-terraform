# logs.tf

# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "locust_log_group" {
  name              = "/ecs/locust-app"
  retention_in_days = 30

  tags = {
    Name = "locust-log-group"
  }
}

resource "aws_cloudwatch_log_stream" "locust_log_stream" {
  name           = "locust-log-stream"
  log_group_name = aws_cloudwatch_log_group.locust_log_group.name
}

