[
  {
    "name": "locust_master",
    "image": "${locust_image}",
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/locust-app",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${locust_port},
        "hostPort": ${locust_port}
      }
    ],
        "environment": [
        {
          "name": "LOCUST_MODE",
          "value": "master"
        }
    ]
  }
]