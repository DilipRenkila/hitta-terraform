[
  {
    "name": "webserver",
    "image": "${webserver_image}",
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${webserver_port},
        "hostPort": ${webserver_port}
      }
    ]
  }
]
