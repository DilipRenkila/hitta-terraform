[
  {
    "name": "locust_slave",
    "image": "${locust_image}",
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/cb-app",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${locust_port},
        "hostPort": ${locust_port}
      }
    ],
    "environment": [
        {
        "name": "MASTER_HOST",
        "value": "${locust_master_host}"
        },
        {
          "name": "LOCUST_MODE",
          "value": "slave"
        }
    ]
  }
]