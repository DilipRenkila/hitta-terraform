# outputs.tf

output "webserver_alb_hostname" {
  value = aws_alb.webserver.dns_name
}

output "locust_master_alb_hostname" {
  value = aws_alb.locust_master.dns_name
}

